#!/usr/bin/env bash

PICS=(building.pnm busy-intersection.pnm  people1.pnm  people2.pnm red-car.pnm  shelf.pnm)
SIGMAS=(2 5 8)
TAUS=(6.7 28.3 45.3)
taus=(6.7 16.7 26.7 11.3 28.3 45.3)
MODES=(cpu gpu omp)

PIC_BASE_PATH="./pictures/"
QUICKSHIFT_BIN="./delta/quickshift"

CSVFILE="experiment-data-$(date "+%F-%T").csv"
POWER_RECORDS="power_readings.txt"
POWERSTRIP_LOGIN="ubnt@txps1.cs.rutgers.edu"


# it would be nice to understand the difference between * and @ for array access.
for pic in ${PICS[*]} ; do
    for sigma in ${SIGMAS[*]} ; do
	for tau in ${TAUS[*]} ; do
	    for mode in ${MODES[*]} ; do
		# record and announce the settings for this experiment
		ITERATION_PARAMETERS="$pic,$sigma,$tau,$mode"
		echo $ITERATION_PARAMETERS
		printf "\n$ITERATION_PARAMETERS," >> $CSVFILE

		# spawn a process to start recording power. Unfortunately, the `sleep` binary
		# on busybox does not allow sub-second arguments, and usleep is not installed.
		printf "" > $POWER_RECORDS  # clear file
		ssh $POWERSTRIP_LOGIN "while sleep 1 ; do cat /proc/power/active_pwr4 ; done" >> $POWER_RECORDS &
		RECORD_PID=$!  # get pid of previously run process.
		echo "ssh return status: $?"

		# run the executable and record the time taken on the fifth colum of the next line
		$QUICKSHIFT_BIN --file $PIC_BASE_PATH$pic --mode $mode --tau $tau --sigma $sigma \
		    | grep "Total time" \
		    | awk '{print $3}' \
		    | tr -dc '[0-9.]' >> $CSVFILE

		# kill said process and get an average of the records therein on the sixth column of the current line
		kill -9 $RECORD_PID
		AVERAGE_POWER=$(python3 ./average.py $POWER_RECORDS)
		printf ",$AVERAGE_POWER" >> $CSVFILE
	    done
	done
    done
done

# cleanup
rm -v $POWER_RECORDS
