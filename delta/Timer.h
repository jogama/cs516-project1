#ifndef __TIMER_H__
#define __TIMER_H__


#include <chrono>

class Timer
{
private:
	std::chrono::high_resolution_clock::time_point m_start;
	std::chrono::high_resolution_clock::time_point m_stop;
	
public:
	Timer();
	void Start();
	void Stop();
	double GetDurationInMS() const;
};


#endif
