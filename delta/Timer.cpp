#include "Timer.h"

Timer::Timer()
{
	m_start = std::chrono::high_resolution_clock::now();
	m_stop = std::chrono::high_resolution_clock::now();
}

void Timer::Start()
{
	m_start = std::chrono::high_resolution_clock::now();
}

void Timer::Stop()
{
	m_stop = std::chrono::high_resolution_clock::now();
}

double Timer::GetDurationInMS() const
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(m_stop - m_start).count() / 1000000.0;
}
