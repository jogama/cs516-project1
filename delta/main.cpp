#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include "quickshift_common.h"
#include "Image.h"
#include "Exception.h"
#include "Timer.h"

void write_image(image_t im, const char * filename)
{
	/********** Copy from matlab style **********/
	Image IMGOUT(im.K > 1 ? Image::RGB : Image::L, im.N2, im.N1);
	for(int k = 0; k < im.K; k++)
		for(int col = 0; col < im.N2; col++)
			for(int row = 0; row < im.N1; row++)
			{
				/* Row transpose */
				unsigned char * pt = IMGOUT.getPixelPt(col, im.N1-1-row);
				/* scale 0-255 */
				pt[k] = (unsigned char) (im.I[row + col*im.N1 + k*im.N1*im.N2]/32*255);
			}

	/********** Write image **********/
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		throw Exception("Could not open the file");
	}
	ofs<<IMGOUT;
}

image_t imseg(image_t im, int * flatmap)
{
	/********** Mean Color **********/
	float * meancolor = (float *) calloc(im.N1*im.N2*im.K, sizeof(float)) ;
	float * counts    = (float *) calloc(im.N1*im.N2, sizeof(float)) ;

	for (int p = 0; p < im.N1*im.N2; p++) {
		counts[flatmap[p]]++;
		for (int k = 0; k < im.K; k++)
			meancolor[flatmap[p] + k*im.N1*im.N2] += im.I[p + k*im.N1*im.N2];
	}

	int roots = 0;
	for (int p = 0; p < im.N1*im.N2; p++) {
		if (flatmap[p] == p)
			roots++;
	}
	//printf("Roots: %d\n", roots);

	int nonzero = 0;
	for (int p = 0; p < im.N1*im.N2; p++) {
		if (counts[p] > 0) {
			nonzero++;
			for (int k = 0; k < im.K; k++)
				meancolor[p + k*im.N1*im.N2] /= counts[p];
		}
	}
	//if (roots != nonzero)
	//	printf("Nonzero: %d\n", nonzero);
	/* assert(roots == nonzero); --Uli  */

	/********** Create output image **********/
	image_t imout = im;
	imout.I = (float *) calloc(im.N1*im.N2*im.K, sizeof(float));
	for (int p = 0; p < im.N1*im.N2; p++)
		for (int k = 0; k < im.K; k++)
			imout.I[p + k*im.N1*im.N2] = meancolor[flatmap[p] + k*im.N1*im.N2];

	free(meancolor);
	free(counts);

	return imout;
}

int * map_to_flatmap(float * map, unsigned int size)
{
  /********** Flatmap **********/
	int *flatmap      = (int *) malloc(size*sizeof(int)) ;
	for (unsigned int p = 0; p < size; p++) {
		flatmap[p] = map[p];
	}

	bool changed = true;
	while (changed) {
		changed = false;
		for (unsigned int p = 0; p < size; p++) {
			changed = changed || (flatmap[p] != flatmap[flatmap[p]]);
			flatmap[p] = flatmap[flatmap[p]];
		}
	}

	/* Consistency check */
	/* --Uli
	for (unsigned int p = 0; p < size; p++)
	   assert(flatmap[p] == flatmap[flatmap[p]]); 
	*/
	return flatmap;
}

void image_to_matlab(Image & IMG, image_t & im)
{
	/********** Convert image to MATLAB style representation **********/
	im.N1 = IMG.getHeight();
	im.N2 = IMG.getWidth();
	im.K  = IMG.getPixelSize();
	im.I = (float *) calloc(im.N1*im.N2*im.K, sizeof(float));
	for(int k = 0; k < im.K; k++)
		for(int col = 0; col < im.N2; col++)
			for(int row = 0; row < im.N1; row++) {
				unsigned char * pt = IMG.getPixelPt(col, im.N1-1-row);
				im.I[row + col*im.N1 + k*im.N1*im.N2] = 32. * pt[k] / 255.; // Scale 0-32
			}
}


bool getCmdLineArgStr(int argc, const char **argv, const std::string& key, std::string& val)
{
	for(int i=0; i < argc; i++) {
		if(std::string(argv[i]).find(key) != std::string::npos)
			if(i < argc-1) {
				val = std::string(argv[i+1]);
				return true;
			}
	}
	return false;
}

bool getCmdLineArgF(int argc, const char **argv, const std::string& key, float& val)
{
	for(int i=0; i < argc; i++) {
		if(std::string(argv[i]).find(key) != std::string::npos)
			if(i < argc-1) {
				val = std::stof(std::string(argv[i+1]));
				return true;
			}
	}
	return false;
}


int main(int argc, char ** argv)
{
	//Use command-line specified CUDA device, otherwise use device with highest Gflops/s
	if(argc <= 1) {
		printf("Usage: QuickShift [--file inputImage] [--mode mmm] [--outfile outputImage] [--tau tauVal] [--sigma sigmaVal] \n");
		printf("        mode: cpu/omp/gpu. Default is gpu\n");
		printf("        tau: determine the pixel density distance. Default is 10\n");
		printf("        sigma: determine the pazen window size. Default is 6. Smaller sigma gives more disjoint regions, and vice versa\n");
		exit(0);
	}
	float sigma = 6, tau = 10;
	std::string file("flowers2.pnm");
	std::string mode("gpu");
	std::string outover;
	std::string tstr; float tmp;
	if(getCmdLineArgStr(argc, (const char**) argv, "file", tstr))
		file = tstr;
	if(getCmdLineArgStr(argc, (const char**) argv, "mode", tstr))
		mode = tstr;
	if(getCmdLineArgStr(argc, (const char**) argv, "outfile", tstr))
		outover = tstr;
	if(getCmdLineArgF(argc, (const char**) argv, "tau", tmp))
		tau = tmp;
	if(getCmdLineArgF(argc, (const char**) argv, "sigma", tmp))
		sigma = tmp;

	/********** Read image **********/
	Image IMG;
	char outfile[1024];

	std::ifstream ifs(file, std::ios::binary);
	if (!ifs) {
		throw Exception("Could not open the file");
	}
	ifs >> IMG;
	image_t im;

	image_to_matlab(IMG, im);

	// initialize CUDA device
	if(mode == "gpu")
		initializeCUDA(0);

	Timer total_time, compute_time;
	total_time.Start();

	/********** setup **********/
	float *map, *E, *gaps;
	int * flatmap;
	image_t imout;

	map          = (float *) calloc(im.N1*im.N2, sizeof(float)) ;
	gaps         = (float *) calloc(im.N1*im.N2, sizeof(float)) ;
	E            = (float *) calloc(im.N1*im.N2, sizeof(float)) ;

	compute_time.Start();

	/********** Quick shift **********/
	if(mode == "cpu") {
		printf("Mode: CPU single-thread\n");
		quickshift(im, sigma, tau, map, gaps, E);
	} else if(mode == "omp") {
		printf("Mode: CPU multi-thread OpenMP \n");
		quickshift_mp(im, sigma, tau, map, gaps, E);
	} else if(mode == "gpu") {
		printf("Mode: GPU CUDA\n");
		quickshift_gpu(im, sigma, tau, map, gaps, E);
	} else
	   printf("Assert missing; there is a problem\n");
	  /* assert(0 && "Unrecognized mode line"); -- Uli */

	compute_time.Stop();
	printf("Time: %fms\n\n", compute_time.GetDurationInMS());
			
	/* Consistency check */
	/*   --Uli 
	for(int p = 0; p < im.N1*im.N2; p++)
		if(map[p] == p) assert(gaps[p] == INF);
	*/
	flatmap = map_to_flatmap(map, im.N1*im.N2);
	imout = imseg(im, flatmap);
    
	sprintf(outfile, "%s", file.c_str());
//	char * c = strrchr(outfile, '.');
	//mod by Liu
	char * c = outfile;
	int offset =-1;
	for(int i = 0; i<1024; i++){
		if(outfile[i]=='.')offset=i;
	}
	c = c+offset;
	//end of mod;
	if(c) *c = '\0';
		sprintf(outfile, "%s-%s.pnm", outfile, mode.c_str()); 

	if(!outover.empty())
		write_image(imout, outover.c_str());
	else
		write_image(imout, outfile);

	free(flatmap);
	free(imout.I);

	total_time.Stop();
	printf("Total time: %fms\n\n", total_time.GetDurationInMS());

	/********** Cleanup **********/
	free(im.I);
	free(map);
	free(E);
	free(gaps);
}
