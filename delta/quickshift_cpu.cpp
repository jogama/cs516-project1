/** @internal
 ** @file:       quickshift.cpp
 ** @author:     Brian Fulkerson
 ** @author:     Andrea Vedaldi
 ** @brief:      Quickshift command line 
 **/

#include <math.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "quickshift_common.h"
#include "Timer.h"
#include <omp.h>


void quickshift(image_t im, float sigma, float tau, float * map, float * gaps, float * E)
{
	int verb = 1 ;

	float tau2, sigma2x2;
  
	int K, d;
	int N1,N2, i1,i2, j1,j2, R, tR;

	float const * I = im.I;
	N1 = im.N1;
	N2 = im.N2;
	K = im.K;

	d = 2 + K ; /* Total dimensions include spatial component (x,y) */
  
	tau2  = tau*tau;
	sigma2x2 = sigma*sigma*2;
  
	R = (int) ceil (3 * sigma) ;
	tR = (int) ceil (tau) ;
  
	if (verb) {
		printf("quickshift: [N1,N2,K]: [%d,%d,%d]\n", N1,N2,K) ;
		printf("quickshift: sigma:   %g\n", sigma) ;
		/* R is ceil(3 * sigma) and determines the window size to accumulate
		* similarity */
		printf("quickshift: R:       %d\n", R) ; 
		printf("quickshift: tau:     %g\n", tau) ;
		printf("quickshift: tR:      %d\n", tR) ;
	}

  /* -----------------------------------------------------------------
   *                                                                 n 
   * -------------------------------------------------------------- */

  	Timer timer;
	timer.Start();

  /* -----------------------------------------------------------------
   *                                                 E = - [oN'*F]', M
   * -------------------------------------------------------------- */
  
  /* 
     D_ij = d(x_i,x_j)
     E_ij = exp(- .5 * D_ij / sigma^2) ;
     F_ij = - E_ij             
     E_i  = sum_j E_ij
     M_di = sum_j X_j F_ij

     E is the parzen window estimate of the density
     0 = dissimilar to everything, windowsize = identical
  */
	int N1xN2 = N1*N2;
	int *kN1N2 = (int*) malloc (sizeof(int)*K);
	for(int k=0; k < K; k++) kN1N2[k] = k*N1xN2;
  
	for (i2 = 0 ; i2 < N2 ; ++ i2) {
		int i2N1 = i2*N1;
		for (i1 = 0 ; i1 < N1 ; ++ i1) {
			float Ei = 0;
			int j1min = VL_MAX(i1 - R, 0   ) ;
			int j1max = VL_MIN(i1 + R, N1-1) ;
			int j2min = VL_MAX(i2 - R, 0   ) ;
			int j2max = VL_MIN(i2 + R, N2-1) ;      
      
			/* For each pixel in the window compute the distance between it and the
			* source pixel */
			for (j2 = j2min ; j2 <= j2max ; ++ j2) {
				int j2N1 = j2*N1;
				for (j1 = j1min ; j1 <= j1max ; ++ j1) {
					float Dij = (i1-j1)*(i1-j1) + (i2-j2)*(i2-j2);
					for(int k=0; k < K; k++) {
						Dij += (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]) 
							* (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]);
					}
					/* Make distance a similarity */ 
					float Fij = exp(- Dij / sigma2x2) ;

					/* E is E_i above */
					Ei += Fij; 
				} /* j1 */ 
			} /* j2 */
			/* Normalize */
			E [i1 + i2N1] = Ei / ((j1max-j1min)*(j2max-j2min));
      
			/*E [i1 + N1 * i2] = Ei ; */
		}  /* i1 */
	} /* i2 */
  
	timer.Stop();
	printf("ComputeE: %fms\n", timer.GetDurationInMS());

	timer.Start();
 
  /* -----------------------------------------------------------------
   *                                               Find best neighbors
   * -------------------------------------------------------------- */
  
	/* Quickshift assigns each i to the closest j which has an increase in the
     * density (E). If there is no j s.t. Ej > Ei, then gaps_i == inf (a root
     * node in one of the trees of merges).
     */
	for (i2 = 0 ; i2 < N2 ; ++i2) {
		int i2N1 = i2*N1;
		for (i1 = 0 ; i1 < N1 ; ++i1) {
			float E0 = E [i1 + N1 * i2] ;
			float d_best = INF ;
			float j1_best = i1   ;
			float j2_best = i2   ; 
        
			int j1min = VL_MAX(i1 - tR, 0   ) ;
			int j1max = VL_MIN(i1 + tR, N1-1) ;
			int j2min = VL_MAX(i2 - tR, 0   ) ;
			int j2max = VL_MIN(i2 + tR, N2-1) ;      
        
			for (j2 = j2min ; j2 <= j2max ; ++ j2) {
				int j2N1 = j2*N1;
				for (j1 = j1min ; j1 <= j1max ; ++ j1) {            
					if (E [j1 + N1 * j2] > E0) {
						float Dij = (i1-j1)*(i1-j1) + (i2-j2)*(i2-j2);
						for(int k=0; k < K; k++) {
							Dij += (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]) 
								* (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]);
						}         
						if (Dij <= tau2 && Dij < d_best) {
							d_best = Dij ;
							j1_best = j1 ;
							j2_best = j2 ;
						}
					}
				}
			}
        
    /* map is the index of the best pair */
    /* gaps_i is the minimal distance, inf implies no Ej > Ei within
        * distance tau from the point */
			map [i1 + i2N1] = j1_best + N1 * j2_best ; /* + 1 ; */
			if (map[i1 + i2N1] != i1 + i2N1)
				gaps[i1 + i2N1] = sqrt(d_best) ;
			else
				gaps[i1 + i2N1] = d_best; /* inf */
		}
	}  
	
	timer.Stop();
	printf("ComputeN: %fms\n", timer.GetDurationInMS());

	free(kN1N2);
}

void quickshift_mp(image_t im, float sigma, float tau, float * map, float * gaps, float * E)
{
	int verb = 1 ;

	float tau2, sigma2x2;
  
	int K, d;
	int N1,N2, R, tR;

	float const * I = im.I;
	N1 = im.N1;
	N2 = im.N2;
	K = im.K;

	d = 2 + K ; /* Total dimensions include spatial component (x,y) */
  
        omp_set_num_threads(4); /* hard coded to 4 threads */

	tau2  = tau*tau;
	sigma2x2 = sigma*sigma*2;
  
	R = (int) ceil (3 * sigma) ;
	tR = (int) ceil (tau) ;
  
	if (verb) {
		printf("quickshiftMP: [N1,N2,K]: [%d,%d,%d]\n", N1,N2,K) ;
		printf("quickshiftMP: sigma:   %g\n", sigma) ;
		/* R is ceil(3 * sigma) and determines the window size to accumulate
		* similarity */
		printf("quickshiftMP: R:       %d\n", R) ; 
		printf("quickshiftMP: tau:     %g\n", tau) ;
		printf("quickshiftMP: tR:      %d\n", tR) ;
	}

  /* -----------------------------------------------------------------
   *                                                                 n 
   * -------------------------------------------------------------- */

  	Timer timer;
	timer.Start();

  /* -----------------------------------------------------------------
   *                                                 E = - [oN'*F]', M
   * -------------------------------------------------------------- */
  
  /* 
     D_ij = d(x_i,x_j)
     E_ij = exp(- .5 * D_ij / sigma^2) ;
     F_ij = - E_ij             
     E_i  = sum_j E_ij
     M_di = sum_j X_j F_ij

     E is the parzen window estimate of the density
     0 = dissimilar to everything, windowsize = identical
  */
	int N1xN2 = N1*N2;
	int *kN1N2 = (int*) malloc (sizeof(int)*K);
	for(int k=0; k < K; k++) kN1N2[k] = k*N1xN2;
  
	#pragma omp parallel for 
	for (int i2 = 0 ; i2 < N2 ; ++ i2) {
		int i2N1 = i2*N1;
		for (int i1 = 0 ; i1 < N1 ; ++ i1) {
			float Ei = 0;
			int j1min = VL_MAX(i1 - R, 0   ) ;
			int j1max = VL_MIN(i1 + R, N1-1) ;
			int j2min = VL_MAX(i2 - R, 0   ) ;
			int j2max = VL_MIN(i2 + R, N2-1) ;      
      
			/* For each pixel in the window compute the distance between it and the
			* source pixel */
			for (int j2 = j2min ; j2 <= j2max ; ++ j2) {
				int j2N1 = j2*N1;
				for (int j1 = j1min ; j1 <= j1max ; ++ j1) {
					float Dij = (i1-j1)*(i1-j1) + (i2-j2)*(i2-j2);
					for(int k=0; k < K; k++) {
						Dij += (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]) 
							* (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]);
					}
					/* Make distance a similarity */ 
					float Fij = exp(- Dij / sigma2x2) ;

					/* E is E_i above */
					Ei += Fij; 
				} /* j1 */ 
			} /* j2 */
			/* Normalize */
			E [i1 + i2N1] = Ei / ((j1max-j1min)*(j2max-j2min));
      
			/*E [i1 + N1 * i2] = Ei ; */
		}  /* i1 */
	} /* i2 */
  
	timer.Stop();
	printf("ComputeE: %fms\n", timer.GetDurationInMS());

	timer.Start();
 
  /* -----------------------------------------------------------------
   *                                               Find best neighbors
   * -------------------------------------------------------------- */
  
	/* Quickshift assigns each i to the closest j which has an increase in the
     * density (E). If there is no j s.t. Ej > Ei, then gaps_i == inf (a root
     * node in one of the trees of merges).
     */
	#pragma omp parallel for
	for (int i2 = 0 ; i2 < N2 ; ++i2) {
		int i2N1 = i2*N1;
		for (int i1 = 0 ; i1 < N1 ; ++i1) {
			float E0 = E [i1 + N1 * i2] ;
			float d_best = INF ;
			float j1_best = i1   ;
			float j2_best = i2   ; 
        
			int j1min = VL_MAX(i1 - tR, 0   ) ;
			int j1max = VL_MIN(i1 + tR, N1-1) ;
			int j2min = VL_MAX(i2 - tR, 0   ) ;
			int j2max = VL_MIN(i2 + tR, N2-1) ;      
        
			for (int j2 = j2min ; j2 <= j2max ; ++ j2) {
				int j2N1 = j2*N1;
				for (int j1 = j1min ; j1 <= j1max ; ++ j1) {            
					if (E [j1 + N1 * j2] > E0) {
						float Dij = (i1-j1)*(i1-j1) + (i2-j2)*(i2-j2);
						for(int k=0; k < K; k++) {
							Dij += (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]) 
								* (I[i1 + i2N1 + kN1N2[k]] - I[j1 + j2N1 + kN1N2[k]]);
						}         
						if (Dij <= tau2 && Dij < d_best) {
							d_best = Dij ;
							j1_best = j1 ;
							j2_best = j2 ;
						}
					}
				}
			}
        
    /* map is the index of the best pair */
    /* gaps_i is the minimal distance, inf implies no Ej > Ei within
        * distance tau from the point */
			map [i1 + i2N1] = j1_best + N1 * j2_best ; /* + 1 ; */
			if (map[i1 + i2N1] != i1 + i2N1)
				gaps[i1 + i2N1] = sqrt(d_best) ;
			else
				gaps[i1 + i2N1] = d_best; /* inf */
		}
	}  
	
	timer.Stop();
	printf("ComputeN: %fms\n", timer.GetDurationInMS());

	free(kN1N2);
}
