#!/usr/bin/env python3

import csv
import sys

# NOTE: The TX-1 is running Python 3.5

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Expect exacly one argument, the csv file path!")
        exit(-1)
        
    filepath = sys.argv[1]
    total_sum = 0.
    datum_count = 0
    
    with open(filepath) as csvfile:
        numreader = csv.reader(csvfile)

        for row in numreader:
            total_sum += float(row[0])  # rows of power recording file should be of length 1.
            datum_count += 1

    # print average
    if datum_count != 0:
        print(total_sum / datum_count)
    else:
        print("NO DATA IN {}".format(filepath))

    # Exit happily
    exit(1)
